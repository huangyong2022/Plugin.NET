﻿namespace PluginNET.events
{
    /// <summary>
    /// 插件加载错误事件
    /// </summary>
    /// <param name="e">事件参数型</param>
    public delegate void PluginErrorEvent(PluginErrorEventArgs e);
}
